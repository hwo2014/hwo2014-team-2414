namespace bot.Infrastructure
{
    public class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "yellow";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
}
