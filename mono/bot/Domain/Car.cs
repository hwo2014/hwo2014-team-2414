﻿namespace bot.Domain
{
    public class Car
    {
        private string _name;
        private string _color;

        //private float _length;
        //private float _width;
        //private float _flagPosition;

        public Car(string name, string color)
        {
            _name = name;
            _color = color;
        }

        public string Name
        {
            get { return _name; }
        }

        public string Color
        {
            get { return _color; }
        }
    }
}