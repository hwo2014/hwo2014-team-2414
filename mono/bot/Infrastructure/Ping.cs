namespace bot.Infrastructure
{
    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}

