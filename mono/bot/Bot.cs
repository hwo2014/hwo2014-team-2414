using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using bot;
using bot.Domain;
using bot.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot
{
    private StreamWriter writer;
    
    private Race _race = new Race();
    private string _trackId;
    private string _trackName;
    private Track _track;
    private string _carName;
    private string _carColor;

    

    public Bot()
    {
        //LOG
        if (LoggerState != LoggerStateEnum.NoDebug)
            CreateLogFile();
    }

    public Bot(StreamReader reader, StreamWriter writer, Join join):this()
    {
        this.writer = writer;
        string line;

        send(join);

        JObject rss;

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = recev(line);
            //Console.WriteLine(string.Format("{0}:{1}",msg.msgType,msg.gameTick));
            switch (msg.msgType)
            {
                case "carPositions":
                    //Console.WriteLine(msg.data.ToString());

                    if (msg.gameTick != null)
                    {
                        var rsa = JArray.Parse(msg.data.ToString());
                        var throttle = GetThrottle(rsa);
                        send(new Throttle(throttle, (int)msg.gameTick));
                    }
                    else
                    {
                        send(new Ping());
                    }
                    
                    //Console.WriteLine(msg.data.ToString());
                    /*if (botState == LoggerStateEnum.Debug1)
                        AppendToFile(string.Format(@"""{0}"":", msg.msgType), string.Format("{0},", msg.data));
                    else if (botState == LoggerStateEnum.Debug2)
                    {
                        this._data.Add(string.Format(@"""{0}"":", msg.msgType));
                        this._data.Add(string.Format("{0},", msg.data));
                    }*/

                    break;
                case "join":
                    Console.WriteLine("Joined");
                    //Console.WriteLine(msg.data.ToString());

                    rss = JObject.Parse(msg.data.ToString());
                    RegisterTeam( rss );
                    
                    //RegisterCompetition(rss);
                    //CreateLogFile(msg);
                    /*if (botState == LoggerStateEnum.Debug1)
                        AppendToFile( string.Format(@"""{0}"":", msg.msgType), string.Format("{0},", msg.data) );
                    else if (botState == LoggerStateEnum.Debug2)
                    {
                        this._data.Add(string.Format(@"""{0}"":", msg.msgType));
                        this._data.Add(string.Format("{0},", msg.data));
                    }*/

                    send(new Ping());
                    break;
                case "gameInit":
                    Console.WriteLine("Race init");
                    //Console.WriteLine(msg.data.ToString());

                    rss = JObject.Parse(msg.data.ToString());
                    RegisterTrack(rss);

                    //RegisterPartsIntoTrack(msg);
                    /*if (botState == LoggerStateEnum.Debug1)
                        AppendToFile(string.Format(@"""{0}"":", msg.msgType), string.Format("{0},", msg.data));
                    else if (botState == LoggerStateEnum.Debug2)
                    {
                        this._data.Add(string.Format(@"""{0}"":", msg.msgType));
                        this._data.Add(string.Format("{0},", msg.data));
                    }*/

                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");

                    /*if (botState == LoggerStateEnum.Debug1)
                        AppendToFile(string.Format(@"""{0}"":", msg.msgType), string.Format("{0},", msg.data));
                    else if (botState == LoggerStateEnum.Debug2)
                    {
                        this._data.Add(string.Format(@"""{0}"":", msg.msgType));
                        this._data.Add(string.Format("{0},", msg.data));
                    }*/

                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");

                    /*if (botState == LoggerStateEnum.Debug1)
                        AppendToFile(string.Format(@"""{0}"":", msg.msgType), "{},");
                    else if (botState == LoggerStateEnum.Debug2)
                    {
                        this._data.Add(string.Format(@"""{0}"":", msg.msgType));
                        this._data.Add("{},");
                    }*/

                    send(new Ping());
                    break;
                default:
                    //Console.WriteLine(string.Format("{0},", msg.msgType));

                    //AppendToFile(string.Format(@"""{0}"":", msg.msgType), "{},");
                    //this._data.Add(string.Format(@"""{0}"":", msg.msgType));
                    //this._data.Add("{},");
                    //this._data.Add(string.Format("{0},", msg.data));

                    send(new Ping());
                    break;
            }
        }
        
        /*
        if (botState == LoggerStateEnum.Debug2)
            WriteToFile(this._data.ToString()); //finish
        */
    }

    private void RegisterTeam(JObject rss)
    {
        this._carName = rss["name"].ToString();
        this._carColor = rss["name"].ToString();
    }
    private float GetThrottle(JArray rsa)
    {
        int cars = rsa.Count;
        float throttle = 0.6f;
        Part part = null;
        for (int i=0; i < cars; i++)
        {
            if (rsa[i]["id"]["name"].ToString() == this._carName)
            {
                double carAngle = double.Parse(rsa[i]["angle"].ToString());
                int partId = int.Parse(rsa[i]["piecePosition"]["pieceIndex"].ToString());

                part = this._track.FindPart(partId);
                throttle = part.GetRecommendedThrottle();
                //Console.WriteLine(string.Format("{0}=>{1}",part.Angle,throttle)); //Debug
                return throttle;
            }
        }
        return throttle; //Default value
    }
    private void RegisterTrack(JObject rss)
    {
        this._trackId = rss["race"]["track"]["id"].ToString();
        this._trackName = rss["race"]["track"]["name"].ToString();

        this._race.AddTrack(_trackId, _trackName);

        this._track = this._race.FindTrackById(_trackId);

        RegisterParts(rss);

        RegisterCars(rss);
    }
    private void RegisterParts(JObject rss)
    {
        JToken parts = rss["race"]["track"]["pieces"];

        Track track = this._race.FindTrackById(this._trackId); 
        //string _trackId = rss["race"]["track"]["id"].ToString();

        for (int i = 0; i < parts.Count(); i++)
        {
            JToken token = parts[i];

            float length = 0.0f;
            if (token["length"] != null)
                length = float.Parse(token["length"].ToString());

            double angle = 0.0d;
            if (token["angle"] != null)
                angle = double.Parse(token["angle"].ToString());

            bool switcher = false;
            if (token["switch"] != null)
                switcher = bool.Parse(token["switch"].ToString().ToLower());

            track.AddPart(i, angle, length, switcher);
        }
    }
    private void RegisterCars(JObject rss)
    {
        string raceid = rss["race"]["track"]["id"].ToString();

        Track track = this._race.FindTrackById(raceid);

        JToken cars = rss["race"]["cars"];

        for (int i = 0; i < cars.Count() ; i++)
        {
            JToken token = cars[i];

            string name = token["id"]["name"].ToString();
            string color = token["id"]["color"].ToString();

            this._race.AddCar(name, color);

        }
    }



    //==========================================================================================
    //LOG
    //==========================================================================================
    private string _basepath = @"C:\Log";
    private string _filename;
    private const LoggerStateEnum LoggerState = LoggerStateEnum.NoDebug;

    private MsgWrapper recev(string line)
    {
        /*
        var jsonSettings = new JsonSerializerSettings {ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,NullValueHandling = NullValueHandling.Ignore, };
        MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line, jsonSettings);
        return msg;
        */

        //LOG
        if (LoggerState == LoggerStateEnum.DebugBoth || LoggerState == LoggerStateEnum.DebugRecv)
            AppendToFile(string.Format("recv:{0}", line));
        
        return MsgWrapper.FromJson(line);
    }

    private void send(SendMsg msg)
    {
        var line = msg.ToJson();
        writer.WriteLine(line);

        //LOG
        if (LoggerState == LoggerStateEnum.DebugBoth || LoggerState == LoggerStateEnum.DebugSend)
            AppendToFile(string.Format("send:{0}", line));
    }

    //used to respond to messages that has tick
    private void send(SendMsgWithTick msg)
    {
        var line = msg.ToJson();

        writer.WriteLine(line);

        //LOG
        if (LoggerState == LoggerStateEnum.DebugBoth || LoggerState == LoggerStateEnum.DebugSend)
            AppendToFile(string.Format("send:{0}", line));
    }

    
    private void CreateLogFile()
    {
        string filename = string.Format("{0}.txt", DateTime.Now.ToString("yyyyMMdd_HHmmss"));

        this._basepath = Path.Combine(Directory.GetCurrentDirectory(), "log");
        this._filename = Path.Combine(this._basepath, filename);
        
        if (!Directory.Exists(this._basepath))
            Directory.CreateDirectory(this._basepath);
    }
    private void AppendToFile(string line)
    {
        using (StreamWriter file = File.AppendText(this._filename))
        {
            file.WriteLine(line);
        }
    }











    /**
     * Game Init
     */
    private void RegisterPartsIntoTrack(MsgWrapper msg)
    {
        /*JObject rss = JObject.Parse(msg.data.ToString());
        JToken parts = rss["race"]["track"]["pieces"];

        this._track = new Track(rss["race"]["track"]["id"]);

        for (int i = 0; i < parts.Count(); i++)
        {
            JToken token = parts[i];
            var keys = token.Values<string>();
            
            float length = 0.0f;
            if (token["length"] != null)
                length = float.Parse(token["length"].ToString());

            double angle = 0.0d;
            if (token["angle"] != null)
                angle = double.Parse(token["angle"].ToString());

            bool switcher = false;
            if (token["switch"] != null)
                switcher = bool.Parse(token["switch"].ToString().ToLower());

            _track.AddPart(i, angle, length, switcher);
        }*/
    }
    private float GetOptimalThrottleFor(MsgWrapper msg)
    {
        //JObject rss = JObject.Parse(msg.data.ToString());
        //JToken pieces = rss["id"].First(x=> x["name"] == team);
        return 0.0f;
    }

    private void WriteToFile(string data)
    {
        /*this._data.Add(data);
        using (StreamWriter file = new StreamWriter(this._filename))
        {
            foreach (var line in _data)
            {
                file.WriteLine(line);
            }
        }*/
    }
}