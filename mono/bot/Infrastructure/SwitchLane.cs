﻿namespace bot.Infrastructure
{
    public class SwitchLane : SendMsg
    {
        public string value; //left or right

        public SwitchLane(string value)
        {
            this.value = value;
        }

        protected override object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}