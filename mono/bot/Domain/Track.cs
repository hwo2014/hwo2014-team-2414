﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace bot.Domain
{
    public class Track
    {
        private string _id;
        private string _name; 
        private IList<Part> _parts;

        public Track( string id, string name )
        {
            this._id = id;
            this._name = name;
            this._parts = new List<Part>();
        }

        public void AddPart( int index, double angle, float distance, bool hasSwitch )
        {
            this._parts.Add( new Part( index, angle, distance, hasSwitch ) );
        }

        public Part FindPart(int index)
        {
            return this._parts.First( x => x.Index == index );
        }

        public string Id
        {
            get { return _id; }
        }
        public string Name
        {
            get { return _name; }
        }

    }
}