﻿/*
  Angle  =>  Throttle Range
[   0   ] =>       1.00
[01 - 10] => 0.9 - 0.99
[11 - 20] => 0.8 - 0.89
[21 - 30] => 0.7 - 0.79
[31 - 40] => 0.6 - 0.69
[41 - 50] => 0.5 - 0.59
[51 - 60] => 0.4 - 0.49
[61 - 70] => 0.3 - 0.39
[71 - 80] => 0.2 - 0.29
[81 - 90] => 0.1 - 0.19
[91 -100] => 0.0
*/

using System;

namespace bot.Domain
{
    public class Part
    {
        private int _index;
        private PieceTypeEnum _pieceType;
        private double _angle;
        private float _distance;
        private float _maxT; //maximumThroottle allowed
        private float _minT; //minimumThroottle allowed
        private bool _switchLanes;

        public Part(int index, double angle, float distance, bool hasSwitch)
        {
            this._index = index;
            this._angle = angle;
            this._distance = distance;
            this._switchLanes = hasSwitch;

            if (Math.Abs(angle) < 1.0)
            {
                this._pieceType = PieceTypeEnum.Straights;
            }
            else if (angle > 0 && angle <= 9)
            {
                this._pieceType = PieceTypeEnum.Bends;
            }
            
            CalculateTrottleBounds(angle);
        }

        public static float CalculateUpperBoundThrottle(double angle)
        {
            var decade = 10.0*Math.Floor(Math.Abs(angle)/10);
            var decadeMinRange = (100.0 - (decade + 1.0));
            var decadePercent = decadeMinRange / 100.0;
            return (float)decadePercent;
        }

        public static float CalculateLowerBoundThrottle(double angle)
        {
            var decade = 10.0 * Math.Floor(Math.Abs(angle) / 10);
            var decadeMinRange = (100.0 - (decade + 10.0));
            var decadePercent = decadeMinRange / 100.0;
            return (float)decadePercent;
        }

        private void CalculateTrottleBounds(double angle)
        {
            if (Math.Abs(angle) < 1)
            {
                this._minT = 0.8F;
                this._maxT = 0.8F;
            }
            else
            {
                this._maxT = CalculateLowerBoundThrottle(angle);
                this._minT = CalculateUpperBoundThrottle(angle);
            }
        }

        public float GetRecommendedThrottle()
        {
            if (this._switchLanes)
                return 0.4f;

            return MinT;
            //return (MinT+MaxT)/2;
        }
        
        public int Index
        {
            get { return _index; }
        }
        public PieceTypeEnum PieceType
        {
            get { return _pieceType; }
        }
        public double Angle
        {
            get { return _angle; }
        }
        public float Distance
        {
            get { return _distance; }
        }
        public float MaxT
        {
            get { return _maxT; }
        }
        public float MinT
        {
            get { return _minT; }
        }
        public bool SwitchLanes
        {
            get { return _switchLanes; }
        }
    }

    public enum PieceTypeEnum
    {
        Straights,
        Bends,
    }
}