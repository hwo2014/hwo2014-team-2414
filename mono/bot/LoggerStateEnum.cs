﻿namespace bot
{
    public enum LoggerStateEnum
    {
        DebugSend, //Append each step to Log
        DebugRecv,
        DebugBoth,
        NoDebug, //No Log
    }
}