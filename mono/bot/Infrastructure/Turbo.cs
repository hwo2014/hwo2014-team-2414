﻿using System;

namespace bot.Infrastructure
{
    public class Turbo : SendMsg
    {
        public string value = "WWRRRooooooooooommmmmm !!!...♪♫♪♫ !!! ...♪♫♪♫ !!! ...♪♫♪♫ !!!";

        public Turbo(string value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }
}