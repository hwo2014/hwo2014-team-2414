using System;
using Newtonsoft.Json;

namespace bot.Infrastructure
{
    public class MsgWrapper
    {
        public string msgType;
        public Object data;
        //public string gameId;
        public int? gameTick;

        private MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
        
        public MsgWrapper(string msgType, Object data, int? gameTick)
            :this(msgType,data)
        {
            this.gameTick = gameTick;
        }

        public static MsgWrapper FromJson(string line
            )
        {
            var jsonSettings = new JsonSerializerSettings
            {
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                NullValueHandling = NullValueHandling.Ignore,
            };
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line, jsonSettings);
            return msg;

        }
    }
}
