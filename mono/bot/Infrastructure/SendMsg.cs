
using System;
using Newtonsoft.Json;

namespace bot.Infrastructure
{
    public abstract class SendMsg
    {
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData(), null));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }
}

