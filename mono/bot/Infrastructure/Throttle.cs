using System;

namespace bot.Infrastructure
{
    public class Throttle : SendMsgWithTick
    {
        public double value;

        public Throttle(double value, int? tick)
        {
            this.value = value;
            this.tick = tick;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
        
        protected override int? GameTick()
        {
            return this.tick;
        }
    }
}

