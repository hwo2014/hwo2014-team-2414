﻿using System.Collections.Generic;
using System.Linq;

namespace bot.Domain
{
    public class Race
    {
        private IList<Track> _track;
        private IList<Car> _cars;

        public Race()
        {
            _track = new List<Track>(); 
            _cars = new List<Car>();
        }

        //================Tracks=========================
        public Track FindTrackByName(string name)
        {
            return this._track.First(x=>x.Name == name);
        }

        public Track FindTrackById(string id)
        {
            return this._track.First(x => x.Id == id);
        }

        public void AddTrack(string id, string name)
        {
            var track = new Track(id, name);
            
            this._track.Add(track);
        }
        //================Cars=========================
        public Car FindCarByName(string name)
        {
            return null;
        }

        public void AddCar(string name, string color)
        {
            var car = new Car(name, color);

            this._cars.Add(car);
        }
    }
}