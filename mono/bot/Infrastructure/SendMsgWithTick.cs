﻿using Newtonsoft.Json;

namespace bot.Infrastructure
{
    public abstract class SendMsgWithTick : SendMsg
    {
        protected int? tick;

        protected abstract int? GameTick();
        
        public override string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData(), this.GameTick()));
        }
    }
}